NAME
    zs-json.pl - A JSON client for Zaaksysteem

SYNOPSIS
    zs-json.pl OPTIONS

  OPTIONS
    help
        This help

    url The base URL of Zaaksysteem, required

    platform_key
        The platform key to use for authentication

    path
        The path you want to throw your request at

    zql ZQL statement

COPYRIGHT and LICENSE
    Copyright (c) 2014, 2018, Mintlab B.V. All rights reserved

