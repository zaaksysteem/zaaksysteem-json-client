#! perl
use strict;
use warnings;

use Test::More;
use Test::Exception;

use File::Spec::Functions qw(catfile);
use JSON;
use autodie;

use Zaaksysteem::JSON::Client;
use HTTP::Request;
use Test::LWP::UserAgent;

{
    my $url       = "http://testsuite";
    my $api_ident = 42;
    my @keys      = qw(at comment next num_rows prev result rows status_code);

    my $ua = Test::LWP::UserAgent->new;

    my %data;
    my $json = encode_json(\%data);

    my $zs = Zaaksysteem::JSON::Client->new(
        platform_key => 'lalala',
        url       => $url,
        ua        => $ua,
        api_ident => $api_ident,
    );

    pass("We must succeed");

    $ua->unmap_all();

    $ua->map_response(
        qr{api/v1/iets},
        HTTP::Response->new(
            '200', 'OK', ['Content-Type' => 'application/json'], $json
        )
    );

}

done_testing;
