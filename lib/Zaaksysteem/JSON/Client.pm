package Zaaksysteem::JSON::Client;
use Moose;
use namespace::autoclean;

use HTTP::Cookies;
use JSON;
use LWP::UserAgent;
use URI;
use Storable qw(freeze);

our $VERSION = 0.2;

has [qw(url platform_key)] => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'ua' => (
    lazy     => 1,
    is       => 'ro',
    isa      => 'LWP::UserAgent',
    required => 0,
    default  => sub {
        my $self    = shift;
        my $agent   = LWP::UserAgent->new(
            agent                 => "Zaaksysteem::JSON:Client/$VERSION",
            ssl_opts              => {
                verify_hostname  => 0,
            },
            requests_redirectable => [qw(POST GET HEAD)],
            cookie_jar            => HTTP::Cookies->new({}),
            protocols_allowed     => [qw(http https)],
            timeout               => 10,
        );

        $agent->default_header('ZS-Platform-Key' => $self->platform_key);

        return $agent;
    },
);

has binary => (
    is      => 'rw',
    isa     => 'Bool',
    default => 0,
);

has verbose => (
    is      => 'rw',
    isa     => 'Bool',
    default => 0,
);

sub search {
    my ($self, $path, $zql, %query) = @_;
    $query{zql} = $zql;
    $query{zapi_no_pager} = 1;
    return $self->get($path, %query);
}

sub get {
    my ($self, $path, %query) = @_;
    return $self->_parse_api_response($self->ua->get($self->_generate_uri($path, %query)));
}

sub _generate_uri {
    my ($self, $path, %query) = @_;
    my $URI = URI->new_abs($path, $self->url);
    $URI->query_form(%query) if (keys %query);
    print STDERR $URI . "\n" if $self->verbose;
    return $URI;
}

sub post {
    my ($self, $path, $data, %query) = @_;

    my $content_type = 'form-data';

    if ($self->binary)  {
        $data = freeze($data);
        $content_type = 'application/json';
    }
    return $self->_parse_api_response(
        $self->ua->post(
            $self->_generate_uri($path, %query),
            Content_Type => $content_type,
            Content      => $data,
        )
    );
}

sub _parse_api_response {
    my $self = shift;
    my ($response) = @_;

    if (!$response->is_success) {
        die(sprintf("Unable to parse response for URI %s: %s\n", $response->request->uri, $response->status_line));
    }

    if ($response->content_type ne 'application/json') {
        print $response->content if $self->verbose;
        die("Not a JSON response!\n");
    }

    my $json = $response->content;
    print STDERR $json if $self->verbose;
    $json = decode_json($json);

    my @expected_keys = qw(at comment next num_rows prev result rows status_code);
    if (keys %$json != @expected_keys) {
        die("Invalid JSON response, expected keys are not available\n");
    }

    return $json;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::JSON::Client - A JSON client for Zaaksysteem

=head1 SYNOPSIS

    use Zaaksysteem::JSON::Client;

    my $zs = Zaaksysteem::JSON::Client->new(
        url      => 'https://10.44.0.11',
        ua       => LWP::UserAgent->new(), #optional
    );

    my ($object) = $zs->post('api/object/save', { stuff => 'here'});
    my @objects  = $zs->search('api/object/search', 'SELECT {} FROM domain');
    my @objects  = $zs->get('api/object/search?zql=SELECT {} FROM domain');

=head1 ATTRIBUTES

=head2 url

The base url for all the calls.

=head2 ua

An LWP::UserAgent object, we have a default you may use.

=head1 METHODS

=head2 search

Convenience method for get("PATH?zql=ZQL");

=head3 ARGUMENTS

=over

=item * path

=item * zql

=back

=head3 RETURNS

=head2 get

=over

=item * path

=back

=head2 post

=over

=item * path

=item * data

=back

=head2 login

Login to zaaksysteem

=head1 BUGS

=head1 AUTHOR

    Wesley Schwengle
    CPAN ID: WATERKIP
    Mintlab B.V. http://www.mintlab.nl
    wesley@mintlab.nl

=head1 COPYRIGHT

Copyright 2014, Mintlab B.V. All rights reserved.

=head1 SEE ALSO

perl(1).

=cut
